package org.autoclick.tracking;

import java.util.logging.Level;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class DriverManager {
	
	private WebDriver driver;
	
	public WebDriver getWebDriver() {
		if (driver == null) 
			driver = this.initWebDriver();
		return driver;
	}

	private WebDriver initWebDriver() {
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, this.getPreferences());		
		WebDriver chromeDriver = new FirefoxDriver(capabilities);
		return chromeDriver;
	}
	
	public EventFiringWebDriver initFiringWebDriver(WebDriver driver) {
		EventFiringWebDriver firingEventDriver = new EventFiringWebDriver(driver);
		return firingEventDriver;
	}
	
	private LoggingPreferences getPreferences() {
		LoggingPreferences logs = new LoggingPreferences();
		logs.enable(LogType.BROWSER, Level.OFF);
		logs.enable(LogType.CLIENT, Level.OFF);
		logs.enable(LogType.DRIVER, Level.OFF);
		logs.enable(LogType.PERFORMANCE, Level.OFF);
		logs.enable(LogType.PROFILER, Level.OFF);
		logs.enable(LogType.SERVER, Level.OFF);
		return logs;
	}
}
