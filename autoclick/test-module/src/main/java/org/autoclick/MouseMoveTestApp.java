package org.autoclick;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MouseMoveTestApp {
	
	public static void main(String[] args) throws AWTException, InterruptedException {
		WebDriver driver = new FirefoxDriver();
		driver.get("file:///D:/Projects/AutoClick/autoclick/test-module/src/main/webtest/index.html");
		driver.findElement(By.tagName("body")).sendKeys(Keys.F11);
		Robot robot = new Robot();
		int x = 0, y = 50;
		for (int i = 0; i < 10; i++) {
			robot.mouseMove(x, y);
			x += 50;
			y += 50;
			Thread.sleep(2000L);
		}		
	}

}
