drop database if exists `at_clickdata`;

create database `at_clickdata` default character set `utf8` default collate `utf8_general_ci`;
use `at_clickdata`;

create table `at_user` (
	`id` int not null auto_increment,
	`login` varchar(50) not null,
	`pass_digest` char(36) not null,
	`creation_date` datetime not null,
	`last_login` datetime,
	`email` varchar(100) not null,
	`active` boolean,
	constraint `PK_AT_User_ID` primary key(`id`)
);

create table `at_admin` (
	`id` int not null auto_increment primary key,
	`login` varchar(25) not null,
	`pass_digest` varchar(25) not null
);

create table `at_website` (
	`id` int not null auto_increment,
	`web_url` varchar(1000) not null,
	`active` boolean,
	`creation_date` datetime,
	`server_time` datetime not null,
	constraint `PK_Website_ID` primary key(`id`)
);

create table `at_web_click` (
	`id` char(36) not null,
	`website` int not null,
	`parent` int not null,
	`rel_path` varchar(1000) not null,
	`element_style` varchar(500) not null,
	`bg_image` longblob not null,
	`dest_pos_x` int not null,
	`dest_pos_y` int not null,
	constraint `PK_Web_Click_ID` primary key(`id`),
	constraint `Fk_Web_Client_Website` foreign key(`website`) references `at_website`(`id`) on delete cascade on update cascade
);

create table `at_user_websites_data` (
	`id` int not null auto_increment,
	`user_id` int not null,
	`website_id` int not null,
	`user_login` varchar(255) not null,
	`user_pass` varchar(255) not null,
	constraint `PK_User_Websites_Data_ID` primary key(`id`),
	constraint `Fk_User_Websites_Data_User` foreign key(`user_id`) references `at_user`(`id`) on delete cascade
);


